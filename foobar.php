#!/usr/bin/php
<?php

if (php_sapi_name() !== 'cli') {
    exit;
}

class App
{
    public function runCommand()
    {
        static::printEmptyLine(2);
        for ($i = 1; $i < 100; $i++) {
            if ($i % 15 == 0) {
                static::printFooBar();
            } elseif ($i % 5 == 0) {
                static::printBar();
            } elseif ($i % 3 == 0) {
                static::printFoo();
            } else {
                static::printValue($i);
            }
        }
        static::printEmptyLine(2);
    }

    public static function printValue($value)
    {
        echo $value . ", ";
    }

    public static function printFoo()
    {
        echo "foo, ";
    }

    public static function printBar()
    {
        echo "bar, ";
    }

    public static function printFooBar()
    {
        echo "foobar, ";
    }

    public static function printEmptyLine($quantity)
    {
        for ($i = 0; $i < $quantity; $i++) {
            echo "\n";
        }
    }
}

$app = new App();
$app->runCommand($argv);
